module BlackScholesCalculation
	extend ActiveSupport::Concern

  def ndist(z)
    (1.0/(Math.sqrt(2*Math::PI)))*Math.exp(-0.5*z)
  end

  def n(z)
    b1 =  0.31938153
    b2 = -0.356563782
    b3 =  1.781477937
    b4 = -1.821255978
    b5 =  1.330274429
    p  =  0.2316419
    c2 =  0.3989423

    a = z.abs

    return 1.0 if a > 6.0

    t = 1.0/(1.0+a*p)
    b = c2*Math.exp((-z)*(z/2.0))
    n = ((((b5*t+b4)*t+b3)*t+b2)*t+b1)*t
    n = 1.0-b*n

    n = 1.0 - n if z < 0.0

    n
  end

  def black_scholes(c,s,x,r,v,t)
    sqt = Math.sqrt(t)

    d1 = (Math.log(s/x) + r*t)/(v*sqt) + 0.5*(v*sqt)
    d2 = d1 - (v*sqt)

    if c == 'call'
      delta = n(d1)
      nd2 = n(d2)
    else
      delta = -n(-d1)
      nd2 = -n(-d2)
    end

    ert = Math.exp(-r*t)
    nd1 = ndist(d1)

    gamma = nd1/(s*v*sqt)
    vega = s*sqt*nd1
    theta = -(s*v*nd1)/(2*sqt) - r*x*ert*nd2
    rho = x*t*ert*nd2

    price = s*delta-x*ert*nd2

    {price: price, gamma: gamma, vega: vega, theta: theta, rho: rho, delta: delta}
  end 
end