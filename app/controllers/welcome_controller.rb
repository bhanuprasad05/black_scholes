class WelcomeController < ApplicationController

  include BlackScholesCalculation
  include ActionView::Helpers::NumberHelper

  def index

  end

  def calculate_black_scholes
  	c = params[:call_or_put]
  	s = params[:stock_price].to_i
  	x = params[:strike_price].to_i
  	r = params[:interest_rate].to_i
  	v = params[:volatility].to_i
  	t = (DateTime.strptime(params[:maturity_date], "%m/%d/%Y %H:%M %P").mjd - DateTime.now.mjd)/356
  	@a = black_scholes(c,s,x,r,v,t)
  end
end
